package com.example.bookmarks.domain;

public record FindBookmarksQuery(int pageNo, int pageSize) {
}

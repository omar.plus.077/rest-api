package com.example.bookmarks.domain;

import jakarta.persistence.*;

import java.time.Instant;

@Entity
@Table(name = "bookmarks")
class Bookmark {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(nullable = false)
  private String title;
  @Column(nullable = false)
  private String url;
  @Column(name = "created_at", nullable = false, updatable = false)
  private Instant createdAt;
  @Column(name = "updated_at", insertable = false)
  private Instant updatedAt;

  public Bookmark() {
  }

  Long getId() {
    return id;
  }

  void setId(Long id) {
    this.id = id;
  }

  String getTitle() {
    return title;
  }

  void setTitle(String title) {
    this.title = title;
  }

  String getUrl() {
    return url;
  }

  void setUrl(String url) {
    this.url = url;
  }

  Instant getCreatedAt() {
    return createdAt;
  }

  void setCreatedAt(Instant createdAt) {
    this.createdAt = createdAt;
  }

  Instant getUpdatedAt() {
    return updatedAt;
  }

  void setUpdatedAt(Instant updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Bookmark bookmark)) return false;

    if (getId() != null ? !getId().equals(bookmark.getId()) : bookmark.getId() != null) return false;
    if (getTitle() != null ? !getTitle().equals(bookmark.getTitle()) : bookmark.getTitle() != null) return false;
    return getUrl() != null ? getUrl().equals(bookmark.getUrl()) : bookmark.getUrl() == null;
  }

  @Override
  public int hashCode() {
    int result = getId() != null ? getId().hashCode() : 0;
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    result = 31 * result + (getUrl() != null ? getUrl().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Bookmark{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", url='" + url + '\'' +
        ", createdAt=" + createdAt +
        ", updatedAt=" + updatedAt +
        '}';
  }
}

create table bookmarks
(
  id         bigserial primary key,
  title      varchar not null,
  url        varchar not null,
  created_at timestamp,
  updated_at timestamp
);

INSERT INTO bookmarks(title, url, created_at) VALUES
('Recommended Resources on Testing Java Applications','https://rieckpil.de/recommended-resources-for-testing-java-applications/', CURRENT_TIMESTAMP),
('PostgreSQL Wiki!','https://wiki.postgresql.org/wiki/Main_Page', CURRENT_TIMESTAMP),
('joebew42/study-path','https://github.com/joebew42/study-path', CURRENT_TIMESTAMP),
('aws-knowledge-center','https://repost.aws/knowledge-center/all?view=all&sort=votes&page=eyJ2IjoyLCJuIjoialdOSnNodWZwamxTb0hoelBTQkFlZz09IiwidCI6IjMyZHNHQ0lNc3l2QWpObjFHWTd0L1E9PSJ9&pageSize=60', CURRENT_TIMESTAMP),
('Introduction To Numeric Constants In Go','https://www.ardanlabs.com/blog/2014/04/introduction-to-numeric-constants-in-go.html', CURRENT_TIMESTAMP),
('JAVA, SQL AND JOOQ.','https://blog.jooq.org/', CURRENT_TIMESTAMP),
('Relational Databases Explained','https://architecturenotes.co/things-you-should-know-about-databases/', CURRENT_TIMESTAMP),
('Load Balancing','https://samwho.dev/load-balancing/', CURRENT_TIMESTAMP),
('All the resources you ever need as a Java & Spring application developer','https://sivalabs.in/all-the-resources-you-ever-need-as-a-java-spring-application-developer', CURRENT_TIMESTAMP),
('CS 253 Web Security','https://web.stanford.edu/class/cs253/', CURRENT_TIMESTAMP),
('API design guide','https://cloud.google.com/apis/design', CURRENT_TIMESTAMP),
('Java Pub House','https://www.javapubhouse.com/episodes', CURRENT_TIMESTAMP),
('Deploy to AWS from GitLab CI/CD','https://docs.gitlab.com/ee/ci/cloud_deployment/', CURRENT_TIMESTAMP),
('Working with Errors in Go 1.13','https://go.dev/blog/go1.13-errors', CURRENT_TIMESTAMP),
('Serverless Architectures','https://martinfowler.com/articles/serverless.html', CURRENT_TIMESTAMP)
;
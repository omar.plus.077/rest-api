TRUNCATE TABLE bookmarks;
ALTER SEQUENCE bookmarks_id_seq RESTART WITH 1;

INSERT INTO bookmarks(title, url, created_at) VALUES
('Recommended Resources on Testing Java Applications','https://rieckpil.de/recommended-resources-for-testing-java-applications/', CURRENT_TIMESTAMP),
('PostgreSQL Wiki!','https://wiki.postgresql.org/wiki/Main_Page', CURRENT_TIMESTAMP),
('joebew42/study-path','https://github.com/joebew42/study-path', CURRENT_TIMESTAMP),
('aws-knowledge-center','https://repost.aws/knowledge-center/all?view=all&sort=votes&page=eyJ2IjoyLCJuIjoialdOSnNodWZwamxTb0hoelBTQkFlZz09IiwidCI6IjMyZHNHQ0lNc3l2QWpObjFHWTd0L1E9PSJ9&pageSize=60', CURRENT_TIMESTAMP),
('Introduction To Numeric Constants In Go','https://www.ardanlabs.com/blog/2014/04/introduction-to-numeric-constants-in-go.html', CURRENT_TIMESTAMP);

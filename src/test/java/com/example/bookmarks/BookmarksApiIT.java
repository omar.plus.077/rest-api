package com.example.bookmarks;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookmarksApiIT {

  @LocalServerPort
  private Integer port;



  @BeforeEach
  void setUp() {
    RestAssured.port = port;

  }

  @Test
  @Sql("/test-data.sql")
  void shouldGetBookmarksByPage() {
    given()
        .accept(ContentType.JSON)
    .when()
        .get("/actuator/health")
    .then()
        .statusCode(200)
        .body("status", equalTo("UP"));
  }
}

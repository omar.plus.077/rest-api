package com.example.bookmarks;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.document;
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.documentationConfiguration;


@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(RestDocumentationExtension.class)
class BookmarksApiTest {

  @LocalServerPort
  private Integer port;

  @ServiceConnection
  @Container
  static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15.3-alpine")
      .withDatabaseName("bookmarks")
      .withUsername("bookmarks")
      .withPassword("bookmarks");


  private RequestSpecification spec = null;

  @BeforeEach
  void setUp(RestDocumentationContextProvider restDocumentation) {
    RestAssured.port = port;
    spec = new RequestSpecBuilder()
        .addFilter(documentationConfiguration(restDocumentation))
        .build();

  }

  @Test
  @Sql("/test-data.sql")
  void shouldGetBookmarksByPage() {
    given(spec)
        .accept(ContentType.JSON)
        .filter(document("{method-name}", preprocessResponse(prettyPrint())))
    .when()
        .get("/api/bookmarks?page=1&size=5")
    .then()
        .statusCode(200)
        .body("data.size()", equalTo(5))
        .body("totalElements", equalTo(5))
        .body("pageNumber", equalTo(1))
        .body("totalPages", equalTo(1))
        .body("isFirst", equalTo(true))
        .body("isLast", equalTo(true))
        .body("hasNext", equalTo(false))
        .body("hasPrevious", equalTo(false));
  }
}
